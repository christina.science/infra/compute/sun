# Sun

Setup a control node to run Ansible playbooks

> _sun: radiant energy, especially heat and visible light_

Ubuntu WSL, VM, and bare metal compatible

1. Install Ansible

    ``` bash
    # sudo apt update --yes
    # sudo apt upgrade --yes
    # sudo /sbin/reboot # if necessary
    sudo apt install software-properties-common ansible -y
    ```

1. Clone this repo

    ``` bash
    # WORKSPACE=~/code/deposition/infra/compute
    # mkdir -p $WORKSPACE
    # cd $WORKSPACE
    git clone git@gitlab.com:deposition.cloud/infra/compute/sun.git
    ```

If the repo is already checked out locally

``` bash
# cd sun/
ansible-playbook local.yml -K
```

Run specific tasks using tags

``` bash
ansible-playbook local.yml -K --tags keybase
```

## Use

Install collections and 3rd party roles.

``` bash
ansible-galaxy collection install -r requirements.yaml
ansible-galaxy role install -r requirements.yaml --roles-path roles/
```

For passphrase protected keys, use `ssh-agent`, or even better, try [keychain](https://www.funtoo.org/Keychain)

``` bash
ssh-agent bash
ssh-add ~/.ssh/id_rsa
```

## Reading

* [How To Install and Configure Ansible on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-22-04)
* [Ansible and HashiCorp: Better Together](https://www.hashicorp.com/resources/ansible-terraform-better-together)
* [How to manage your workstation configuration with Ansible](https://opensource.com/article/18/3/manage-workstation-ansible)

## License

Ethically sourced under the [Atmosphere License](https://www.open-austin.org/atmosphere-license/)—like open source, for good.
